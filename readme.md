# Git basics:

## Flow when you make your own project

#### Initialize git repo:
1. Open terminal in root directory.
2. Type command "git init"

#### Add remote to repo:
1. Type command "git remote [remote-name] [url]".
> ex: git remote add origin https://gitlab.com/zapinventory.git

#### How to commit:
1. Type command "git add ."
> The above command will stage your current changes.
2. Type command "git commit -m ['message']'"
> Type your message in short line or two, describing what you have worked on.

#### How to push to remote repository:
1. Type command "git push [remote-name] [branch-name]".
> The above command will push your last committed changes to remote.


## Flow when you are not working on your own project

#### How to clone git repo:
1. Type command "git clone https://gitlab.com/zapinventory.git --[folder-name]"
> The above command will clone a repository for you locally from the remote git server, the --folder-name is optional parameter


#### How to fork and then clone using the above clone command:
> Note only public repositories and for private repositories you need to have atleast read access to fork any repository:
1. Click on fork button appearing on top of repository's page.
> Check link:
https://prnt.sc/vi3wpb
2. When fork is complete then the you will see that repository can be accessed under your username now.
3. Consider fork as you have a local repository for you on the remote server same as the primary repository.
4. Terminology here now is:
    1. Primary repository, ex: https://gitlab.com/zaperp/zapinventory2-frontend
    2. Secondary remote repository. ex: https://gitlab.com/prashantandu/zapinventory2-frontend
> Note: When we clone a repository then a default remote and a default branch is automatically assigned to your local repository.

#### How to commit:
1. Type command "git add ."
> The above command will stage your current changes.
2. Type command "git commit -m ['message']'"
> Type your message in short line or two, describing what you have worked on.

#### How to make your branch:
1. Type command git checkout -b [branch-name]
> Ex: git checkout -b "dev", will create a branch with the name "dev" locally.

#### How to push to remote repository:
1. Type command "git push [remote-name] [branch-name]".
> Ex: git push origin dev, will push your last committed changes to remote.


## Flow to generate PR on Gitlab:
1. When we push to the remote on any gitlab branch the you will see this ticker appearing on your gitlab website.

> Check link:
https://docs.gitlab.com/ee/user/project/merge_requests/img/create_merge_request_button_v12_6.png

2. Or alternatively you can create a PR from left pane by opening the repository's page.

> Check link:
https://prnt.sc/vi3prf

3. Click on "New merge request":

> Check link:
https://prnt.sc/vi3q8x

4. Select branches and check for conflicts:
> When doing this from forked repository please select the target branch of primary repository, when you want to push changes for general availability. Yet you are free to make PR in secondary repository for your own development cycles.

> Check link:
https://docs.gitlab.com/ee/user/project/merge_requests/img/new_merge_request_page_v12_6.png

5. Submit the merge request:

> Check link:
https://prnt.sc/vi3rkl
